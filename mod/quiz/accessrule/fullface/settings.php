<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * FULLFace Quiz access rules settings.
 *
 * @package    quizaccess_fullface
 * @copyright  2020 Daniel Neis Araujo <danielneis@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$settings->add(new admin_setting_configtext(
    'quizaccess_fullface/apiurl',
    new lang_string('apiurl', 'quizaccess_fullface'),
    '',
    ''
));

$settings->add(new admin_setting_configtext(
    'quizaccess_fullface/redirecturl',
    new lang_string('redirecturl', 'quizaccess_fullface'),
    '',
    ''
));

$settings->add(new admin_setting_configtext(
    'quizaccess_fullface/username',
    new lang_string('username', 'quizaccess_fullface'),
    '',
    ''
));

$settings->add(new admin_setting_configtext(
    'quizaccess_fullface/password',
    new lang_string('password', 'quizaccess_fullface'),
    '',
    ''
));

$settings->add(new admin_setting_configtext(
    'quizaccess_fullface/scope',
    new lang_string('scope', 'quizaccess_fullface'),
    '',
    ''
));

$settings->add(new admin_setting_configtext(
    'quizaccess_fullface/time',
    new lang_string('time', 'quizaccess_fullface'),
    '',
    ''
));
